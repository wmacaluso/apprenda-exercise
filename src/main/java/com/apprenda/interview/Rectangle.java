package com.apprenda.interview;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class to represent a two-dimensional rectangle
 *
 * Created by Will Macaluso on 12/4/2015.
 */
public class Rectangle {

    private double x;
    private double y;
    private double height;
    private double width;

    public double getX(){return x;}
    public double getY(){return y;}
    public double getHeight(){return height;}
    public double getWidth(){return width;}
    public double getRightCoord(){return x + width;}
    public double getTopCoord(){return y + height;}

    //Constructor for Rectangle
    public Rectangle(double x, double y, double height, double width){
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }

    //Area of rectangle
    public double getArea(){
        return height*width;
    }

    ///To get the intersection points, first find the (inclusive) rectangle of overlap.
    ///Intersection points are defined as corners of the intersection rectangle
    //that belong to a vertical line on one input rect, and a horiz on the other input rect.
    public ArrayList<Point2D.Double> getIntersectionPoints(Rectangle input){

        ArrayList<Point2D.Double> intersectionPoints = new ArrayList<Point2D.Double>();

        if(isIntersecting(input))
        {
            Rectangle inclusiveIntersection = getInclusiveIntersection(input);

            Point2D.Double down_left = new Point2D.Double(inclusiveIntersection.getX(), inclusiveIntersection.getY());
            Point2D.Double down_right = new Point2D.Double(inclusiveIntersection.getRightCoord(), inclusiveIntersection.getY());
            Point2D.Double up_left = new Point2D.Double(inclusiveIntersection.getX(), inclusiveIntersection.getTopCoord());
            Point2D.Double up_right = new Point2D.Double(inclusiveIntersection.getRightCoord(), inclusiveIntersection.getTopCoord());

            //Check all corners of inclusive intersection rectangle. If a corner is the result of two different
            //rectangle sides, it is an intersection point.

            //Corner 1
            if(isIntersectionPoint(down_left, input)){
                intersectionPoints.add(down_left);
            }

            //Corner 2
            if(isIntersectionPoint(down_right, input)){
                intersectionPoints.add(down_right);
            }

            //Corner 3
            if(isIntersectionPoint(up_left, input)){
                intersectionPoints.add(up_left);
            }

            //Corner 4
            if(isIntersectionPoint(up_right, input)){
                intersectionPoints.add(up_right);
            }
        }

        return intersectionPoints;
    }

    //See notes in function above
    public boolean isIntersectionPoint(Point2D.Double pt, Rectangle in){

        boolean pointLiesOnRect1XLine = pt.getX() == getX() || pt.getX() == getRightCoord();
        boolean pointLiesOnRec1YLine = pt.getY() == getY() || pt.getY() == getTopCoord();
        boolean pointLiesOnRect2XLine = pt.getX() == in.getX() || pt.getX() == in.getRightCoord();
        boolean pointLiesOnRect2YLine = pt.getY() == in.getY() || pt.getY() == in.getTopCoord();

        return (pointLiesOnRec1YLine &&  pointLiesOnRect2XLine) || (pointLiesOnRect1XLine && pointLiesOnRect2YLine);
    }

    //Returns the overlap (will return a zero area rectangle if just adjacent).
    public Rectangle getInclusiveIntersection(Rectangle input){
        double maxX = Math.max(x, input.getX());
        double maxY = Math.max(y, input.getY());

        double minMaxX = Math.min(x + width, input.getX() + input.getWidth());
        double minMaxY = Math.min(y + height, input.getY() + input.getHeight());

        double xIntersect = minMaxX - maxX;
        double yIntersect = minMaxY - maxY;

        if(xIntersect >= 0 && yIntersect >= 0){
            return new Rectangle(maxX, maxY, yIntersect, xIntersect);
        }
        else{
            return null;
        }
    }

    //Two rectangles intersect if they have some area of overlap
    public boolean isIntersecting(Rectangle input){
        Rectangle inclusiveIntersection = getInclusiveIntersection(input);

        if(inclusiveIntersection == null){ return false;}

        if(inclusiveIntersection.getArea() > 0){
                return true;
        }

        return false;
    }

    //Two rectangles are adjacent if they share a set of co-linear points
    public boolean isAdjacent(Rectangle input){
        Rectangle intersection = getInclusiveIntersection(input);

        //If they don't inclusively intersect, return false (inclusive intersection allows for 0 area rectangles).
        if(intersection == null){return false;}

        //They inclusively intersect, so check sides
        if(getX() == input.getX() || getX() == input.getRightCoord() || getRightCoord() == input.getX() || getRightCoord() == input.getRightCoord()){
            return true;
        }

        if(getY() == input.getY() || getY() == input.getTopCoord() || getTopCoord() == input.getY() || getTopCoord() == input.getTopCoord()){
            return true;
        }

        return false;
    }

    //If the area of the intersection equals the area of the rectangle, it is contained (assuming we don't have a zero-area rectangle)
    public boolean isContained(Rectangle input){
        Rectangle intersection = getInclusiveIntersection(input);

        if(intersection == null){return false;}

        return intersection.getArea() == getArea();
    }
}
