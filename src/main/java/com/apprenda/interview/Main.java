package com.apprenda.interview;


/**
 *
 * This assumes that we are working in a Euclidean 2D space (X and Y axis is positive in right, up directions)
 *
 * This also assume that zero-area rectangles are not considered.
 *
 * Created by Will Macaluso on 12/4/2015.
 *
 */
public class Main {

    public static void main(String[] args){

        Rectangle r1 = new Rectangle(0, 0, 10, 10);
        Rectangle r2 = new Rectangle(-1, -1, 10, 10);
        Rectangle r3 = new Rectangle(-1, 5, 1, 20);
        Rectangle r4 = new Rectangle(-1, -1, 5, 10);
        Rectangle r5 = new Rectangle(1, 1, 8, 8);
        Rectangle r6 = new Rectangle(10, 10, 5, 5);
        Rectangle r7 = new Rectangle(10, 5, 5, 5);
        Rectangle r8 = new Rectangle(5, 5, 5, 5);

        //Intersections
        System.out.println("Intersection of r1 with r2: " + r1.getIntersectionPoints(r2).toString());
        System.out.println("Intersection of r1 with r3: " + r1.getIntersectionPoints(r3).toString());
        System.out.println("Intersection of r1 with r4: " + r1.getIntersectionPoints(r4).toString());
        System.out.println("Intersection of r1 with r7: " + r1.getIntersectionPoints(r7).toString());

        //Adjacency
        System.out.println("Adjacency of r1 with r5: " + r1.isAdjacent(r5));
        System.out.println("Adjacency of r1 with r6: " + r1.isAdjacent(r6));
        System.out.println("Adjacency of r1 with r7: " + r1.isAdjacent(r7));
        System.out.println("Adjacency of r1 with r8: " + r1.isAdjacent(r8));

        //Containment
        System.out.println("r2 is contained in r1?: " + r2.isContained(r1));
        System.out.println("r3 is contained in r1?: " + r3.isContained(r1));
        System.out.println("r5 is contained in r1?: " + r5.isContained(r1));
        System.out.println("r6 is contained in r1?: " + r6.isContained(r1));
        System.out.println("r7 is contained in r1?: " + r7.isContained(r1));
        System.out.println("r8 is contained in r1?: " + r8.isContained(r1));


    }
}
